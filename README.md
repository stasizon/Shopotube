# Shopotube API

> **Note:**

> All requests need send to **/connects/connects.php**

> Settings for setup database and admin email: **/classes/_class.config.php**


## Product List

#####Get products:
```javascript
$http.post('/connects/connects.php', {
	type: 'read',
	item: 'product'
});
```

#####Write products:
```javascript
$http.post('/connects/connects.php', {
	type: 'write',
	item: 'product',
	product1: {
		name: 'Example name',
		description: 'Example description',
		price: '100',
		...
	}
	product2: {
		...
	}
});
```

## Products order
```javascript
$http.post('/connects/connects.php', {

	email: 'example@gmail.com',
	from: 'Vasya',
	phone: '88888888',
	[
		'Product name: Example name Amount: 1 Price: 50',
		'Product name: Example name Amount: 1 Price: 50',
		...
	]
});
```