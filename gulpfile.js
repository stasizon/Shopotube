const gulp = require('gulp');
const less = require('gulp-less');
const jade = require('gulp-jade');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const gulpIf = require('gulp-if');
const del = require('del');
const browserSync = require('browser-sync').create();
const notify = require('gulp-notify');
const path = require('path');
const data = require('gulp-data');
const include = require('gulp-include');
const flatten = require('gulp-flatten');
const rename = require("gulp-rename");

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

//---------------------------------------------------------------

const phpFolder = '/home/stasizon/server/mysite.zz/';

// --------------------------------------------------------------

gulp.task('clean', function () {
  return del('app');
});


gulp.task('html', function () {

  return gulp.src('assets/index.jade')
    .pipe(jade({
        pretty: true
    }))
    .pipe(rename('index.php'))
    .pipe(gulp.dest('app/'));

});


gulp.task('styles', function () {

  return gulp.src('assets/index.less')
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(less({
        paths: [ path.join(__dirname, 'less', 'includes') ]
      }))
    .on('error', notify.onError(function (err) {
      return {
        title: 'Less crash',
        message: err.message
      }
    }))
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('app/'));

});


gulp.task('js', function () {
  return gulp.src('assets/index.js')
    .pipe(gulpIf(isDevelopment, sourcemaps.init()))
    .pipe(include())
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('app/js'));
});


gulp.task('img', function () {
  return gulp.src('assets/blocks/**/img/*.+(png|jpg|svg)', {base: 'assets/blocks/**', since: gulp.lastRun('img')})
    .pipe(flatten())
    .pipe(gulp.dest('app/img/'));
});


gulp.task('assets', function () {
  return gulp.src(['assets/fonts/**', 'assets/libs/**'], {base: 'assets', since: gulp.lastRun('assets')})
    .pipe(gulp.dest('app/'));
});


gulp.task('backend', function () {
  return gulp.src('assets/backend/**/*.*')
    .pipe(gulp.dest('app/'));
});


gulp.task('test_php', function () {
  return gulp.src('app/**/*.*')
    .pipe(gulp.dest(phpFolder));
});

// --------------------------------------------------------------

gulp.task('serve', function () {
  browserSync.init({
    server: phpFolder
  });

  browserSync.watch('app/**/*.*').on('change', browserSync.reload);
});

gulp.task('watch', function () {
    gulp.watch(['assets/blocks/**/*.*', 'assets/index.jade'], gulp.series('html', 'test_php'));
    gulp.watch(['assets/blocks/**/*.less', 'assets/index.less'], gulp.series('styles', 'test_php'));
    gulp.watch('assets/blocks/**/img/*.+(png|jpg)', gulp.series('img'));
    gulp.watch('assets/libs/**/*.*', gulp.series('assets'));
    gulp.watch('assets/fonts/**/*.*', gulp.series('assets'));
    gulp.watch('assets/backend/**/*.*', gulp.series('backend', 'test_php'));
    gulp.watch(['assets/js/**/*.js', 'assets/index.js'], gulp.series('js', 'test_php'));
});

gulp.task('build', gulp.series('clean', 'html', 'styles', 'js', 'img', 'assets', 'backend', 'test_php'));

gulp.task('dev', gulp.series('build', gulp.parallel(['watch', 'serve'])));
