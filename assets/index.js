//= require libs/ngDialog.min.js
//= require libs/angular-ui-notification.min.js
//= require libs/angular-file-upload.min.js
//= require libs/angular-video-bg.js
//= require js/navigation/navigation.js
//= require js/shoppingCart/shoppingCart.js
//= require js/productList/productList.js
//= require js/productPage/productPage.js
//= require js/orderPage/orderPage.js
//= require js/baseProduct/baseProduct.js
//= require js/storeInfo/storeInfo.js

(function() {

	var app = angular.module('store', ['products', 'shopping-cart', 'product-page', 'order-page', 'navigation', 'base-product', 'store-info', 'angularVideoBg']);

	app.config(function($httpProvider) {

		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

		$httpProvider.defaults.transformRequest = [function(data) {

		   var param = function(obj) {
			 var query = '';
			 var name, value, fullSubName, subValue, innerObj, i;

			 for(name in obj)
			 {
			   value = obj[name];

			   if(value instanceof Array)
			   {
				 for(i=0; i<value.length; ++i)
				 {
				   subValue = value[i];
				   fullSubName = name + '[' + i + ']';
				   innerObj = {};
				   innerObj[fullSubName] = subValue;
				   query += param(innerObj) + '&';
				 }
			   }
			   else if(value instanceof Object)
			   {
				 for(subName in value)
				 {
				   subValue = value[subName];
				   fullSubName = name + '[' + subName + ']';
				   innerObj = {};
				   innerObj[fullSubName] = subValue;
				   query += param(innerObj) + '&';
				 }
			   }
			   else if(value !== undefined && value !== null)
			   {
				 query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
			   }
			 }

			 return query.length ? query.substr(0, query.length - 1) : query;
		   };

		   return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
		 }];

	});

	app.directive('backImg', function(){
        return function(scope, element, attrs){
            var url = attrs.backImg;
			var size = attrs.backSize;

            element.css({
                'background': 'url(' + url +') no-repeat center',
                'background-size' : size || 'cover'
            });
        };
    });

	app.service('cart', function() {

         var cart = {
             productList: [],
             amount: 0,
             addedIndex: []
         };

         return cart;

     });

	app.service('getYotubeVideoID', ['$sce', function($sce) {

		this.ID = function(url) {

            if (typeof url === 'string' && url != '') {

				var videoID = url.match(/=([\w-]+)/)[1];
	            var videoUrl = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoID);

                 console.log(videoID);

                 return videoID;

			} else {
                 // console.error('bad url ' + url);
            }

 		}

    }]);

})();
