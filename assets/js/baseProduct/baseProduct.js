(function() {

    var app = angular.module('base-product', []);

    app.controller('BaseProductController', ['productList', '$sce', 'cart', function(productList, $sce, cart) {

        this.product = productList[0];

        this.addToCart = function(product, amount) {

            product = productList[0];

            product.amount = amount;
            cart.productList[cart.productList.length] = product;
            cart.amount = cart.amount + 1;

        };

        this.disableButton = function(event) {

            event.srcElement.classList.add('disable');
            event.srcElement.innerHTML = 'Добавлено';

        };


        this.isVideo = function () {
            if (productList[0].image == 'video') {
                return true;
            } else {
                return false;
            }
        }

        this.setVideo = function(url) {

            if (url === true) {

                var videoID = url.match(/=([\w-]+)/)[1];

                var videoUrl = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoID);

                return videoUrl;

            }

        };

    }]);

})();
