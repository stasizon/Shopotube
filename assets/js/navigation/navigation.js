(function() {

    var app = angular.module('navigation', []);

    app.controller('NavigationController', function() {

        var lastScrollTop = 0;

        window.addEventListener("scroll", function() {

           var st = window.pageYOffset || document.documentElement.scrollTop;

           if (st > lastScrollTop){
               document.getElementById("nav").classList.add("nav_hide");
           } else {
               document.getElementById("nav").classList.remove("nav_hide");
           }

           lastScrollTop = st;
           
        }, false);


    });

})();
