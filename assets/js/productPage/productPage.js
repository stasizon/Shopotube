(function() {

    var app = angular.module('product-page', []);

    app.controller('ProductPageController', ['cart', '$sce', function(cart, $sce) {

        this.addToCart = function(product, amount) {

            // console.log(product, amount);

            var index = parseInt(product.$$hashKey.replace(/\D+/g,""));

            if (cart.addedIndex.indexOf(index) == -1) {

                console.log(cart);

                product.amount = amount;
                cart.productList[cart.productList.length] = product;
                cart.amount = cart.amount + 1;
                cart.addedIndex[cart.addedIndex.length] = index;

            } else {

                product.amount = product.amount + amount;
				cart.amount = cart.amount + 0;

            };

        };

        this.isVideo = function (product) {

            if (product == 'video') {
                return true;
            } else {
                return false;
            }
        }

        this.setVideo = function(url) {

            var videoID = url.match(/=([\w-]+)/)[1];

            var videoUrl = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoID);

            return videoUrl;

        };


    }]);

})();
