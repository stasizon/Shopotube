(function() {

    var app = angular.module('order-page', []);

    app.controller('OrderPageController', ['cart', '$http', function(cart, $http) {

        this.sendData = function() {

            var data = {}

            data.type = 'mails';
            data.from = document.getElementsByClassName('orderPage__input')[0].value;
            data.email = document.getElementsByClassName('orderPage__input')[1].value;
            data.phone = document.getElementsByClassName('orderPage__input')[2].value;
            data.products = [];
            // console.log(data);

            for (var i = 0; i < cart.productList.length; i++) {
                data.products[i] = 'Название: ' + cart.productList[i].name + ' ' + 'Количество: ' + cart.productList[i].amount;
                // delete data.productList[i].description;
                // delete data.productList[i].video;
			}

            console.log(data);

            $http.post('/connects/connects.php', data).success(function(response) {
                    console.log(response);
            });
        };

    }]);

})();
