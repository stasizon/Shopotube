(function() {

	var app = angular.module('products', ['ngDialog']);

	app.service('productList', function() {

		var xhr = new XMLHttpRequest();

		var body = 'type=read&item=product';

		xhr.open("POST", '/connects/connects.php', false)
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

		xhr.send(body);

		var productList = JSON.parse(xhr.responseText);

		for (var i = 0; i < productList.length; i++) {
			productList[i].video = productList[i].video.split(',');
		}

		return productList;

	});

	app.controller('FunctionalList', ['$http', 'cart', 'ngDialog', '$sce', 'productList', function($http, cart, ngDialog, $sce, productList) {

		this.disableButton = function(event) {

			event.srcElement.classList.add('disable');

		};

		var list = this;
		list.items = productList;

		list.setVideo = function(url) {

			if (url.match(/=([\w-]+)/) != null) {

				var videoID = url.match(/=([\w-]+)/)[1];
	            var videoUrl = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + videoID);
				
			}

            return videoUrl;

        };

		list.isVideo = function (product) {

			if (product.image == 'video') {
				return true;
			} else {
				return false;
			}
		}

		list.openProductPage = function(index) {

			ngDialog.open({
				template: 'productPage',
				className: 'ngdialog-theme-plain',
                data: list.items[index]
			});

		}

		this.addToCart = function(product, amount) {

            // console.log(product, amount);

            // var index = parseInt(product.$$hashKey.replace(/\D+/g,""));
			//
            // if (cart.addedIndex.indexOf(index) == -1) {
			//
                product.amount = amount;
                cart.productList[cart.productList.length] = product;
                cart.amount = cart.amount + 1;
                // cart.addedIndex[cart.addedIndex.length] = index;
			//
            // } else {

                // product.amount = product.amount + amount;
				// cart.amount = cart.amount + 0;

            // };

        };

	}]);

})();
