(function() {

	var app = angular.module('shopping-cart', []);

    app.controller('PanelController', ['cart', 'ngDialog', function(cart, ngDialog) {

       this.show = function() {
           document.getElementById('cartPanel').classList.remove('cartPanel_hidden');
           document.getElementById('cartPanel').classList.add('cartPanel_visible');
       };

       this.hide = function() {
           document.getElementById('cartPanel').classList.remove('cartPanel_visible');
           document.getElementById('cartPanel').classList.add('cartPanel_hidden');
       };

       this.selectedProducts = cart.productList;

       this.panelSwitchAmount = function() {
           return cart.amount;
       };

       this.openOrderPage = function() {

           ngDialog.open({
               template: 'orderPage',
               className: 'ngdialog-theme-plain',
			   data: 'text'
           });

       };
        
        this.remove = function (product, index, event) {

            if (cart.productList[index].name == document.getElementById('baseProductTitle').innerHTML) {

                document.getElementById('baseProductButton').classList.remove('disable');
                document.getElementById('baseProductButton').innerHTML = 'Добавить в корзину';

            }

            delete cart.productList[index];

            var index = parseInt(product.$$hashKey.replace(/\D+/g,""));

            if (cart.addedIndex.indexOf(index) != -1) {

                delete cart.addedIndex[cart.addedIndex.indexOf(index)];

            } else {

                console.log('=')

            };

            console.log(product, index, event);

            cart.amount --;

            event.path[1].style.display = 'none';
        }

    }]);

})();
