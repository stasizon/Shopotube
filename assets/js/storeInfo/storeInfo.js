(function() {

    var app = angular.module('store-info', []);


    app.controller('InfoController', ['$http', 'getYotubeVideoID', function($http, getYotubeVideoID) {

        var info = function () {

            var xhr = new XMLHttpRequest();

            var body = 'type=read&item=configs';

            xhr.open("POST", '/connects/connects.php', false)
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

            xhr.send(body);

            var info = JSON.parse(xhr.responseText);

            return info;

        };

        this.info = info();

        this.setVideo = function(url) {

            console.log(getYotubeVideoID.ID(url));

			return getYotubeVideoID.ID(url);

        };

    }]);

})();
