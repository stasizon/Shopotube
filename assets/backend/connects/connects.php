<?php
include("../classes/_class.config.php"); //файл с доступами к БД
include("../classes/_class.db.php"); //файл с классом БД

# Класс с доступами к БД
$config = new config;

# Класс Базы данных
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);

$data = array();

$post = $_POST;
$type = $post['type'];
$item = $post['item'];

if($type == 'read'){

    if($item == 'product'){

        $db->Query("SELECT * FROM products ORDER BY id DESC");

        while($row = $db->FetchAssoc()){
            $data[] = $row;
        }

        $str = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $str;

    }elseif($item == 'configs'){

        $db->Query("SELECT * FROM configs");

        while($row = $db->FetchAssoc()){
            $data[] = $row;
        }

        $str = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $str;

    }elseif($item == 'mail'){

        $db->Query("SELECT * FROM mails ORDER BY id DESC");

        while($row = $db->FetchArray()){

            $stack = explode("\r\n", $row['mail']);
            //$row_tovar = stristr($row['mail'], 'Товары:');
            //if(!$row_tovar) $row_tovar = $row['mail'];
            //$stack = explode("\r\n", $row_tovar);
            array_shift($stack);
            $found = 0;

            foreach($stack as $t){

                if(!$row['date']) $row['date'] = stristr($t, 'Дата:') ? ltrim(stristr($t, 'Дата:'),'Дата:') : '';

                if(!$found) {
                    $row_tovar = stristr($t, 'Товары:');
                    if(!$row_tovar) $row_tovar = $t; else $found = true;

                } else{

                    $row['tovars'][] = array('name' => (stristr($t, 'Название') ? ltrim(stristr($t, 'Название'), 'Название:') : $t));
                }
                //$row['date'] = $stack[0];
            }
            $data[] = $row;

        }

        $str = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $str;

    }elseif($item == 'meta'){

        $db->Query("SELECT * FROM metatags ORDER BY id DESC");

        while($row = $db->FetchArray()){
            $data[] = $row;
        }

        $str = json_encode($data, JSON_UNESCAPED_UNICODE);
        echo $str;
    }

}elseif($type == 'write'){

    if($item == 'product'){

        $delete_keys = array('type', 'item');

        $new_post = array();

        foreach ($post as $key => $value) {
            if (!in_array($key, $delete_keys)) {
               $new_post[$key] = $value;
            }
        }

        $countP = count($new_post);

        $db->Query("TRUNCATE products");

        if($countP > 1){

            for($i=1;$countP>=$i;$i++){

                $sqlUpdateFields = array();

                foreach ($new_post['product'.$i] as $Key => $value) {

                    $sqlUpdateFields[] = "`{$Key}` = '{$value}'";

                }

                $sqlUpdateFields = join(', ', $sqlUpdateFields);
                $db->Query("INSERT INTO products SET $sqlUpdateFields");
            }

        }else{

            $sqlUpdateFields = array();

            foreach ($new_post['product'] as $Key => $value) {

                $sqlUpdateFields[] = "`{$Key}` = '{$value}'";

            }

            $sqlUpdateFields = join(', ', $sqlUpdateFields);

            $db->Query("INSERT INTO products SET $sqlUpdateFields");

        }

        echo 'Добавлено!';

    }elseif($item == 'meta'){

        $delete_keys = array('type', 'item');

        $new_post = array();

        foreach ($post as $key => $value) {
            if (!in_array($key, $delete_keys)) {
                $new_post[$key] = $value;
            }
        }


        $sqlUpdateFields = array();

        foreach ($new_post as $Key => $value) {
            $sqlUpdateFields[] = "`{$Key}` = '{$value}'";
        }

        $sqlUpdateFields = join(', ', $sqlUpdateFields);

        $db->Query("UPDATE metatags SET $sqlUpdateFields WHERE id = '1'");

        echo 'Сохранено!';

    }elseif($item == 'configs'){

        $delete_keys = array('type', 'item');

        $new_post = array();

        foreach ($post as $key => $value) {
            if (!in_array($key, $delete_keys)) {
                $new_post[$key] = $value;
            }
        }

        $countP = count($new_post);

        $db->Query("TRUNCATE configs");

        if($countP > 1){

            for($i=1;$countP>=$i;$i++){

                $sqlUpdateFields = array();

                foreach
                ($new_post['configs'.$i] as $Key => $value)

                {
                    $sqlUpdateFields[] = "`{$Key}` = '{$value}'";
                }

                $sqlUpdateFields = join(', ', $sqlUpdateFields);
                $db->Query("INSERT INTO configs SET $sqlUpdateFields");
            }

        }else{

            $sqlUpdateFields = array();

            foreach ($new_post['configs'] as $Key => $value) {

                $sqlUpdateFields[] = "`{$Key}` = '{$value}'";

            }

            $sqlUpdateFields = join(', ', $sqlUpdateFields);

            $db->Query("INSERT INTO configs SET $sqlUpdateFields");

        }


        echo 'Сохранено!';

    }

}elseif($type == 'addrow'){

    $table = $post['table'];
    $name = $post['name'];
    $tableType = $post['tabletype'];

    $db->Query("ALTER TABLE `".$table."` ADD ".$name." ".$tableType." NOT NULL");
    echo 'Поле добавлено!';

}elseif($type == 'readrow'){

    $table = $post['table'];

    $db->Query("SHOW COLUMNS FROM ".$table);

    while($row = $db->FetchAssoc()){
        $columns[] = $row['Field'];
    }

    $str = json_encode($data, JSON_UNESCAPED_UNICODE);
    echo $str;

}elseif($type == 'admconf'){

    $login = $post['login'];
    $pass = md5($post['pass']);

    $db->Query("UPDATE adminconfig SET login = '$login', pass = '$pass' WHERE id = '1'");
    echo 'Сохранено!';

}elseif($type == 'admlogin'){

    $login = $post['login'];
    $pass = md5($post['pass']);

    $db->Query("SELECT * FROM adminconfig WHERE id = '1'");
    $row = $db->FetchArray();

    if($row['login'] == $login && $row['pass'] == $pass){
        @session_start();
        $_SESSION["admin"] = $row["login"];
        echo 'ok';
    }else echo 'Не верно указан логин или пароль!';


}elseif($type == 'mails'){

    $delete_keys = array('type', 'item');

    $new_post = array();

    foreach ($post as $key => $value) {
        if (!in_array($key, $delete_keys)) {
            $new_post[$key] = $value;
        }
    }
    /*
    if($countP > 1){

    for($i=1;$countP>=$i;$i++){

    $sqlUpdateFields = array();

    foreach ($new_post['product'.$i] as $Key => $value)
    {

    $sqlUpdateFields[] = "`{$Key}` = '{$value}'";

    }

    $sqlUpdateFields = join(', ', $sqlUpdateFields);
    $db->Query("INSERT INTO products SET $sqlUpdateFields");
    }

    }else{


    */
    $from = $post['from'];
    $phone = $post['phone'];
    $email = $post['email'];
    $subject = 'Заказ с сайта';

    $sqlUpdateFields = array();

    foreach ($new_post['products'] as $Key => $value) {

        //$sqlUpdateFields[] = "{$Key} = {$value}";
        $sqlUpdateFields[] = $value;

    }

    $sqlUpdateFields = join("\r\n", $sqlUpdateFields);

    $text = "Заказ:\r\n";
    $text .= "Дата: ".date('d.m.Y H:i')."\r\n";
    $text .= "Имя: ".$from."\r\n";
    $text .= "Телефон: ".$phone."\r\n";
    $text .= "E-mail: ".$email."\r\n";
    $text .= "Товары:\r\n";
    $text .= $sqlUpdateFields;

    $db->Query("INSERT INTO mails SET email = '$email',name = '$from',phone = '$phone',mail = '$text'");

    include("../classes/_class.isender.php");
    $isender = new isender;
    $send = $isender->SendMail($config->MailTO, $subject, $text);

    //}



}else echo 'Нет параметра type';

?>
